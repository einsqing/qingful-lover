#qingful-lover


#### 青否小红娘前言

论如何打造一款爆款小程序

1）社交娱乐类小程序

提到爆款小程序，我们就不能忘记“匿名聊聊”。作为最火爆的小程序，匿名聊聊在短短的几个小时候内获得了大量的用户。社交类小程序只要抓住用户的人性弱点，充分利用小程序的分享属性，就能够在短期内实现病毒式传播。

（2）工具类小程序

工具类小程序，也是特别容易火爆的。不管是“车来了”，还是“群应用”，都是很实用，也很火爆的小程序。工具类小程序能够满足用户的需求，而且，使用成本特别低。借助于微信庞大的用户群体，工具类小程序是特别容易吸引用户的。

（3）电商类小程序

说到电商类小程序，就不得不提“星巴克”和“拼多多”。这两个小程序，都是很火爆的电商类小程序。之所以这两个小程序受到用户的认可，是因为迎合了用户热衷分享、扩撒的需求，充分发挥用户的积极性。

------



#### 青否小红娘分析

青否小红娘是社交娱乐型小程序，抓住了90，80了很多没有对象的心理，打造的一款泛直播类型的牵线体验。

------



#### 青否小红娘技术栈

小程序端：websocket

后台：koahubjs websocket

------



#### 青否小红娘案例分享

| ![](http://git.oschina.net/einsqing/qingful-lover/raw/master/docs/mmexport1499428042833.jpg) | ![](http://git.oschina.net/einsqing/qingful-lover/raw/master/docs/mmexport1499426979999.jpg) | ![](http://git.oschina.net/einsqing/qingful-lover/raw/master/docs/mmexport1499432390202.jpg) | ![](http://git.oschina.net/einsqing/qingful-lover/raw/master/docs/mmexport1499428823297.jpg) |
| :--------------------------------------: | ---------------------------------------- | ---------------------------------------- | ---------------------------------------- |
| ![](http://git.oschina.net/einsqing/qingful-lover/raw/master/docs/S70726-16455181.jpg) | ![](http://git.oschina.net/einsqing/qingful-lover/raw/master/docs/S70726-16485852.jpg) | ![](http://git.oschina.net/einsqing/qingful-lover/raw/master/docs/S70726-16490520.jpg) | ![](http://git.oschina.net/einsqing/qingful-lover/raw/master/docs/S70726-16462924.jpg) |



------

### 青否小红娘使用帮助

下载代码之后，通过微信web开发者工具体验

------



### 青否小程序技术支持

http://www.qingful.com