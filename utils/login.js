export default function login() {
  return new Promise((resolve, reject) => {
    wx.login({
      success: function (res) {
        const code = res.code;
        wx.getUserInfo({
          success: function (res) {
            resolve({
              code: code,
              encryptedData: res.encryptedData,
              iv: res.iv
            });
          },
          fail: function (res) {
            wx.openSetting({
              success: function (res) {
                if (res.authSetting['scope.userInfo']) {
                  return resolve(login());
                }
                return reject();
              }
            })
          }
        });
      }
    });
  });

}